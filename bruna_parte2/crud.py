from sqlalchemy.orm import Session
from uuid import UUID
import models, schemas



def get_students(db: Session, students_id: UUID):
    return db.query(models.Students.id == students_id).first()

def get_students_CPF(db: Session, CPF: str(14)):
    return db.query(models.Students.CPF == CPF).first()

def get_all_students(db: Session):
    return db.query(models.Students).all()

def create_students(db: Session, students: schemas.StudentsCreate):
    db_students = models.Students(
        name = students.name, 
        CPF = students.CPF, 
        registration = students.registration, 
        year_of_entry = students.year_of_entry, 
        semester = students.semester, 
        email = students.email)
    db.add(db_students)
    db.commit()
    db.refresh(db_students)   
    return db_students
    
def delete_students():
    return 


def get_professors(db: Session, professors_id: UUID):
    return db.query(models.Professors.id == professors_id).first()

def get_professors_CPF(db: Session, CPF: str(14)):
    return db.query(models.Professors.CPF == CPF).first()

def get_all_professors(db: Session):
    return db.query(models.Professors).all()

def create_professors(db: Session, professors: schemas.ProfessorsCreate):
    db_professors = models.Professors(
        name = professors.name,
        CPF = professors.CPF,
        year_of_entry = professors.year_of_entry,
        degree = professors.degree
    )
    db.add(db_professors)
    db.commit()
    db.refresh(db_professors)
    return db_professors
    


def get_classes(db: Session, classes_id: UUID):
    return db.query(models.Classes.id == classes_id).first()

def get_all_classes(db: Session):
    return db.query(models.Classes).all()

def create_classes(db: Session, classes: schemas.ClassesCreate):
    db_classes = models.Classes(
        name = classes.name,
        description = classes.description
    )
    db.add(db_classes)
    db.commit()
    db.refresh(db_classes)
    return db_classes



def get_courses(db: Session, course_id: UUID):
    return db.query(models.Courses.id == course_id).first()

def get_all_courses(db: Session):
    return db.query(models.Courses).all()

def create_courses(db: Session, courses: schemas.CoursesCreate):
    db_courses = models.Courses(
        name = courses.name,
        workload = courses.workload,
        creation_date = courses.workload
    )
    db.add(db_courses)
    db.commit()
    db.refresh(db_courses)
    return db_courses