from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from uuid import UUID
import uvicorn 
import crud, schemas
from database import get_db


app = FastAPI(
    title = "NEW PROGRAD_API",
    version = "0.0.1",
    contact={
        "name": "PROGRAD UFAL",
        "email": "PROGRADexample@example.com",
    },    
)


@app.get("/students", summary='Fetch all students data', tags=["Students"], response_model=list[schemas.Students])
async def all_data_students(db: Session = Depends(get_db)):
    students = crud.get_students(db)
    return students

@app.post("/students", summary = 'Add a new student', tags=["Students"], response_model=schemas.Students)
async def add_students(students: schemas.StudentsCreate, db: Session = Depends(get_db)):
    db_students = crud.get_students_CPF(db, CPF = students.CPF)
    if db_students:
        raise HTTPException(status_code=404, detail = "CPF already registered")
    return crud.create_students(db=db, students=students)

@app.get("/students/{students_id}", summary = 'Fetch student data', tags=["Students"], response_model=schemas.Students)
async def read_student(students_id: UUID, db: Session = Depends(get_db)):
    db_student = crud.get_students(db, students_id=students_id)
    if db_student is None:
        raise HTTPException(status_code=404, detail = "Student not found registered")
    return db_student
    
@app.put("/students/{students_id}/course/{course_id}", summary = "Set or change student's course", tags=["Students"], response_model = schemas.StudentWithCourseAndClasses,)
async def update_student_course(student_id: UUID, course_id: UUID, db: Session = Depends(get_db)):
    student = crud.read_student(student_id, db)
    crud.read_course(course_id, db)
    db.query(models.Student)\
        .filter(models.Student.id == student_id).update({'course_id': course_id})
    db.commit()
    db.refresh(student) 
    return student

@app.post("/students/{students_id}/class/{class_id}", summary = 'Add student to a class', tags=["Students"], response_model = schemas.StudentWithCourseAndClasses)
async def creat_students_class(student_id: UUID, class_id: UUID, db: Session = Depends(get_db)):
    student = crud.read_student(student_id, db)
    crud.read_course(course_id, db)
    student_class = models.StudentClass(student_id = student_id, class_id = class_id)

    db.add(student_class)
    db.commit()
    db.refresh(student) 
    return student

@app.delete("/students/{students_id}/class/{class_id}", summary = 'Remove student from a class', tags=["Students"], response_model = schemas.StudentWithCourseAndClasses)
async def update_students(student_id: UUID, class_id: UUID, db: Session = Depends(get_db)):
    student = crud.read_student(student_id, db)
    crud.read_course(course_id, db)

    db.query(models.StudentClass)\
        .filter(models.StudentClass.student_id == student_id).filter(models.StudentClass.class_id == class_id).delete()
    db.commit()
    db.refresh(student) 
    return student




@app.get("/professors", summary='Fetch all professors data', tags=["Professors"], response_model=list[schemas.Professors])
async def all_data_professors(db: Session = Depends(get_db)):
    professors = crud.get_professors(db)
    return professors

@app.post("/professors", summary='Add a new professor', tags=["Professors"], response_model=schemas.Professors)
async def creat_professors(professors: schemas.ProfessorsCreate, db: Session = Depends(get_db)):
    db_professors = crud.get_professors_CPF(db, CPF = professors.CPF)
    if db_professors:
        raise HTTPException(status_code=404, detail = "CPF of professor already registered")
    return crud.create_professors(db=db, professors=professors)

@app.get("/professors/{professors_id}", summary='Fetch professor data', tags=["Professors"], response_model=schemas.Professors)
async def read_professors(professors_id: UUID, db: Session = Depends(get_db)):
    db_professors = crud.get_professors(db, professors_id=professors_id)
    if db_professors is None:
        raise HTTPException(status_code=404, detail = "Professors not found registered")
    return db_professors




@app.get("/courses", summary='Fetch all courses data', tags=["Courses"], response_model=list[schemas.Courses])
async def all_data_courses(db: Session = Depends(get_db)):
    courses = crud.get_courses(db)
    return courses

@app.get("/courses/{courses_id}", summary='Fetch course data', tags=["Courses"], response_model=schemas.Courses)
async def read_courses(courses_id: UUID, db: Session = Depends(get_db)):
    db_courses = crud.get_courses(db, courses_id=courses_id)
    if db_courses is None:
        raise HTTPException(status_code=404, detail = "Course not found registered")
    return db_courses

@app.post("/courses", summary='Add a new course', tags=["Courses"], response_model=schemas.Courses)
async def creat_courses(courses: schemas.CoursesCreate, db: Session = Depends(get_db)):
    db_courses = crud.get_courses(db, courses = courses)
    if db_courses:
        raise HTTPException(status_code=404, detail = "Courses already registered")
    return crud.create_courses(db=db, courses=courses)

@app.get("/courses/{courses_id}/students", summary='Fetch students of course', tags=["Courses"], response_model=schemas.Students)
async def read_courses_students(courses_id: UUID, students: schemas.Students, db: Session = Depends(get_db)):
    db_courses_students = crud.get_courses(db, courses_id=courses_id)
    if db_courses_students is None:
        raise HTTPException(status_code=404, detail = "Course not found registered")
    return db_courses_students

@app.put("/courses/{courses_id}/coordinator/{professor_id}", summary='Set or change a professor as the course coordinator', tags=["Courses"], response_model = schemas.CoursesWithProfessor)
async def update_courses_coordinator(courses_id: UUID, professor_id: UUID, db: Session = Depends(get_db)):
    courses = crud.read_courses(courses_id, db)
    crud.read_professor(professor_id, db)
    db.query(models.Courses)\
        .filter(models.Courses.id == Courses_id).update({'professor_id': professor_id})
    db.commit()
    db.refresh(courses) 
    return courses



@app.get("/classes", summary='Fetch all classes data', tags=["Classes"], response_model=list[schemas.Classes])
async def all_data_classes(db: Session = Depends(get_db)):
    classes = crud.get_classes(db)
    return classes

@app.get("/classes/{class_id}", summary='Fetch class data', tags=["Classes"], response_model=schemas.Classes)
async def read_classes(class_id: UUID, db: Session = Depends(get_db)):
    db_classes = crud.get_classes(db, courses_id=class_id)
    if db_classes is None:
        raise HTTPException(status_code=404, detail = "Classes not found registered")
    return db_classes

@app.post("/classes", summary='Add a new class', tags=["Classes"], response_model=schemas.Classes)
async def creat_classes(classes: schemas.ClassesCreate, db: Session = Depends(get_db)):
    db_classes = crud.get_classes(db, classes = classes)
    if db_classes:
        raise HTTPException(status_code=404, detail = "Classes already registered")
    return crud.create_courses(db=db, classes=classes)

@app.get("/classes/{class_id}/students", summary='Fetch students of class', tags=["Classes"], response_model=schemas.Students)
async def read_classes_students(classes_id: UUID, students: schemas.Students, db: Session = Depends(get_db)):
    db_classes_students = crud.get_classes(db, classes_id=classes_id)
    if db_classes_students is None:
        raise HTTPException(status_code=404, detail = "Classes not found registered")
    return db_classes_students

@app.put("/classes/{class_id}/teacher/{professor_id}", summary='Set or change a professor as the class teacher', tags=["Classes"], response_model = schemas.ClassesWithProfessor)
async def update_classes_professor(class_id: UUID, professor_id: UUID, db: Session = Depends(get_db)):
    classes = crud.read_classes(class_id, db)
    crud.read_professor(professor_id, db)
    db.query(models.Classes)\
        .filter(models.Classes.id == Class_id).update({'professor_id': professor_id})
    db.commit()
    db.refresh(classes) 
    return classes

@app.get("/classes/ranking", summary='Get a ranking of classes by number of their enrolled students', tags=["Classes"])
async def read_classes(id: int):
        return "read classes item with id {id}"



if __name__ == "__main__":
    uvicorn.run(app='main:app', host="0.0.0.0", port=8000, reload=True)