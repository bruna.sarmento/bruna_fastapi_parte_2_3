from datetime import date
from uuid import UUID, uuid4
from pydantic import BaseModel


def snake_to_camel(snake_str: str | None = None):
    if snake_str is None:
        return None
    splitted = snake_str.split('_')
    return splitted[0] + ''.join(s.title() for s in splitted[1:])


class GenericModel(BaseModel):
    class Config:
        alias_generator = snake_to_camel
        allow_population_by_field_name = True


class ProfessorsBase(GenericModel):
    name: str
    CPF: str
    year_of_entry: int
    degree: str

class ProfessorsCreate(ProfessorsBase):
    pass

class Professors(ProfessorsBase):
    id: UUID

    class Config:
        orm_mode = True

    

class ClassesBase(GenericModel):  
    name: str
    description: str | None = None

class ClassesCreate(ClassesBase):
    pass

class Classes(ClassesBase):
    id: UUID
    professors: list[Professors] = []

    class Config:
            orm_mode = True



class CoursesBase(GenericModel):  
    name: str
    workload: float
    creation_date: str
    
class CoursesCreate(CoursesBase):
    pass

class Courses(CoursesBase):
    id: UUID

    class Config:
            orm_mode = True



class StudentsBase(GenericModel):   
    name: str
    CPF: str
    registration: int
    year_of_entry: int
    semester: str
    email: str  

class StudentsCreate(StudentsBase):
    pass

class Students(StudentsBase):
    id: UUID
    active_enrollment: bool
    course: list[Courses] = []
    class_: list[Classes] = []

    class Config:
        orm_mode = True




        