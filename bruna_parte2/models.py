from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, UniqueConstraint
from sqlalchemy.types import Date
from sqlalchemy.orm import relationship 
from database import Base
from sqlalchemy.dialects.postgresql import UUID
import uuid


class Student(Base):
    __tablename__ = "student"

    id = Column(UUID, primary_key=True, default=uuid.uuid4, autoincrement=True)
    name = Column(String(300))
    CPF = Column(String(14), unique=True)
    registration = Column(Integer, unique=True)
    year_of_entry = Column(Integer)
    semester = Column(Integer)
    email = Column(String, unique=True)
    active_enrollment = Column(Boolean, default=True)
    
    course_id = Column(UUID, ForeignKey('course.id'))

    ##course = relationship("Course", back_populates = "students")
    classes = relationship("Class", secondary = 'students_class', back_populates = "students")


class Professors(Base):
    __tablename__ = "professors"

    id = Column(UUID, primary_key=True, default= uuid.uuid4, autoincrement=True)
    name = Column(String(300))
    CPF = Column(String(14), unique=True)
    year_of_entry = Column(Integer)
    degree = Column(String(300))

    course = relationship("Course", back_populates = "coordinator")
    classes = relationship("Classes", back_populates = "professors")
    
class Classes(Base):
    __tablename__ = "classes"

    id = Column(UUID, primary_key=True, default= uuid.uuid4, autoincrement=True)
    name = Column(String(300))
    description = Column(String(500)) 

    professor_id = Column(UUID, ForeignKey('professors.id'))

    professor = relationship("Professors", back_populates = "classes")
    students = relationship("Student", secondary='students_class', back_populates = "classes")

class Courses(Base):
    __tablename__ = "courses"

    id = Column(UUID, primary_key=True, default= uuid.uuid4, autoincrement=True)
    name = Column(String(300))
    workload = Column(Integer)
    creation_date = Column(String(10))

    coordinator_id = Column(UUID, ForeignKey('professors.id'))

    coordinator = relationship("Professors", back_populates = "classes")
    students = relationship("Student", back_populates = "course")

class StudentsClass(Base):
    __tablename__ = "students_class"
    __table_args__ = (UniqueConstraint('student_id', 'class_id'),)

    id = Column(UUID, primary_key=True, default= uuid.uuid4, autoincrement=True)

    student_id = Column(UUID, ForeignKey('student.id'))
    class_id = Column(UUID, ForeignKey('classes.id'))
